# TAREA 4: Gestion de ramas
****
## Ejercicio 1:

crear una nueva rama llamada bibliografia mostrar las ramas del repositorio junto con el sha del commit que se creo.

![imagen](imagenes/ejercicio1.png)

## Ejercicio 2: 


1. crear una pagina web llamada capitulo4.html con el siguiente parrafo:

![imagen](imagenes/ejercicio2.png)

![imagen](imagenes/ejercicio2.2.png)

****

2. añadir los cambios al Staging Area.

![imagen](imagenes/ejercicio2-3.png)

****

3. realizar un commit con el mensaje “añadido capitulo 4”

![imagen](imagenes/ejercicio2-33.png)

****
4. mostrar el historial del repositorio incluyendo todas las ramas, de forma grafica.


![imagen](imagenes/ejercicio2-4.png)

****

## Ejercicio 3
****

1. cambiar a la rama bibliografia

![imagen](imagenes/ejercicio3-1.png)
****
2. crear una pagina web en el directorio capitulos llamada bibliografia.html, con el siguente texto.

![imagen](imagenes/ejercicio3-2.png)

![imagen](imagenes/ejercicio3-2par2.png)

****

3. añadir los cambios al Staging Area

![imagen](imagenes/ejercicio3-3.png)

****

4. realizar un commit con el mensaje “añadida primera referencia bibliografica”

![imagen](imagenes/ejercicio3-4.png)

5. mostrar el historial del repositorio incluyendo todas las ramas

![imagen](imagenes/ejercicio3-5.png)
*****
*****

## Ejercicio 4:

1. funsionar la rama bibliografia con la rama master.

![imagen](imagenes/ejercicio4-1.png)

****
2. mostrar el historial del repositorio incluyendo todas las ramas

![imagen](imagenes/ejercicio4-2.png)

****
3. eliminar la rama bibliografia

![imagen](imagenes/ejercicio4-3.png)

****

4. mostrar de nuevo el historial del repositorio incluyendo todas las ramas

![imagen](imagenes/ejercicio4-4.png)
****
## Ejercicio 5:
****
1. crear la rama bibliografia

![imagen](imagenes/ejercicio5-1.png)
****
2. cambiar a la rama bibliografia

![imagen](imagenes/ejercicio5-2.png)

*****

3. cambiar el fichero bibliografia.html para que contenga las siguentes referencias

![imagen](imagenes/ejercicio5-3.png)

*****
4. añadir los cambios al Staging Area y hacer un commit con el mensaje “añadida nueva referencia bibliogràfica”

![imagen](imagenes/ejercicio5-4.png)

*****

5. cambiar a la rama master.

![imagen](imagenes/ejercicio5-5.png)

****

6 cambiar el fichero bibliografia.html para que contenga las siguentes referencias:

![imagen](imagenes/ejercicio5-6.png)

****

7. añadir los cambios al Staging Area y hacer un commit con el mensaje
“añadida nueva referencia bibliográfica”

![imagen](imagenes/ejercicio5-7.png)
****

8. Funsionar la rama bibliografia con la master.

![imagen](imagenes/ejercicio5-8.png)

****

9. resolver el conflicto dejando el fichero bibliografia.html con las referencias:

![imagen](imagenes/ejercicio5-9.png)

![imagen](imagenes/ejercicio5-9par2.png)
****

10. añadir los cambios al Staging Area y hacer commit con el mensaje “Resuelto conflicto de bibliografia”

![imagen](imagenes/ejercicio5-10.png)
****
11. mostrar el historial del repositorio incluyendo todas las ramas.

![imagen](imagenes/ejercicio5-11.png)
